#ifndef PACIENTE_HPP
#define PACIENTE_HPP

#include <iostream>
#include "pessoa.hpp"

class Paciente : public Pessoa {
	// Atributos
	private: 
		int senha;
		string problema;
		float massa_corporal;
		
	public:
		Paciente();
		~Paciente();
		void setSenha(int senha);
		void setProblema(string Problema);
		void setMassaCorporal(float massa_corporal);
		int getSenha();
		string getProblema();
		float getMassaCorporal();
};

#endif
