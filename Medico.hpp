#ifndef MEDICO_HPP
#define MEDICO_HPP

#include <iostream>
#include "pessoa.hpp"

class Medico : public Pessoa {
	// Atributos
	private: 
		int matricula;
		string formacao;
		string area;
	public:
		Medico();
		~Medico();
		void setMatricula(int matricula);
		void setFormacao(string formacao);
		void setArea(string area);
		int getMatricula();
		int getFormacao();
		float getArea();
};

#endif
